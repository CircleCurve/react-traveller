import React, {Component} from 'react' ; 
import {AppRegistry, Image, StatusBar} from 'react-native' ;
import {Left, Icon , Container, Content, Text, List, ListItem} from 'native-base' ; 
//const routes = ["首頁", "路徑" , "Chat", "Profile"] ; 


const routes = [
    {location : "Home" ,  name : "首頁" , icon : "phone-portrait"}, 
    {location : "Plan" ,  name : "行程", icon: "easel",}, 
    {location : "Chat" ,  name : "測試用(Chat)", icon: "notifications",}, 
    {location : "Profile" ,  name : "測試用(Profile)", icon: "radio-button-off",}, 
]


const HeaderImage = "https://lh3.googleusercontent.com/qQn_2gbULn4Jx6NwL7Pfi60-SFi4uI5GzH6VzwRwkSgNsuLT01Y6Q9NuLKOwFOo_dw0=w300" ; 

const HeaderImageStyle = {
              height: 120,
              alignSelf: "stretch",
              justifyContent: "center",
              alignItems: "center"
            }; 


export default class SideBar extends Component {
    render() {
        return (
            <Container>
                <Content>
                    <Image
                      source = {{uri : HeaderImage }}
                      style = {HeaderImageStyle}
                      >
                      
                    </Image>

                    <List 
                        dataArray = {routes}
                        renderRow = {data => {
                            return (
                            
                            <ListItem
                                button 
                                onPress = { () =>   {
                                    if (this.props.activeItemKey !== data.location){
                                        this.props.navigation.navigate(data.location); 
                                    }else{
                                        this.props.navigation.navigate("DrawerClose")
                                    }

                                } }>
                                <Left>
                                    <Icon active name={data.icon} style={{ color: "#777", fontSize: 26, width: 30 }} />
                                    <Text> {data.name} </Text>

								</Left>
                            </ListItem>
                            )
                        }}
                        />
                </Content>
            </Container>
        )
    }
}