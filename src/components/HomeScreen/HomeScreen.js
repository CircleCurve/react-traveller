/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  View,
  Image
} from 'react-native';
import { 
  Container, 
  Header, 
  Title, 
  Left, 
  Icon, 
  Right, 
  Button, 
  Body, 
  Content,
  Text, 
  Card, 
  CardItem, 
  Thumbnail } from "native-base";
import {Col, Row, Grid} from "react-native-easy-grid" ; 

//======Redux ========
import {setName} from '../../actions/userAction' ; 
import {getAttraction} from '../../actions/attractionAction' ; 
import { connect } from 'react-redux';

//======Custom Header========
import {getHeader } from '../HeaderScreen/Header' ; 

class HomeScreen extends Component<{}> {

  constructor (props) {
    super(props) ; 
  }

  componentDidMount () {
    //this.props.getAttraction (this.props.attraction.result) ; 
  }

  render() {
    return (
      <Container>
        { getHeader(this.props.navigation, "Travller") }
        <Content padder>
          {this.props.attraction.result}
          { /*
          <Card>
            <CardItem>
              <Body>
                <Text> Chat App to talk some awesome people !!  {this.props.user.name}  </Text>
              </Body>
            </CardItem>
          </Card>
          
            <Button full rounded primary
            style={{ marginTop : 10 }}
            onPress = { () => 
                //this.props.navigation.navigate("Profile")
                this.props.getAttraction () 
                
            }
            >
              <Text>Goto Profiles</Text>          
          </Button>*/ }
        </Content>
      </Container>
    );
  }
}


const mapStateToProps = (state) => {
    return {
        user: state.user,
        attraction: state.attraction 
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        setName: (name) => {
            dispatch(setName(name));
        },

        getAttraction : (attraction) => {
          dispatch(getAttraction(attraction)) ; 
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen); 