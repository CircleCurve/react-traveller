import React , {Component} from 'react' ; 
import {Header, Left, Body, Title, Right, Icon, Button, Text} from 'native-base' ; 

export const getHeader =  (navigation,name) => 
    <Header androidStatusBarColor="#008080" style={{ backgroundColor : '#008080'  }} >
          <Left>
            <Button 
            transparent
            onPress ={() => navigation.navigate("DrawerOpen")}
            >
            <Icon name="menu" style={{ color : '#FFFFFF'}} />
            </Button>
          </Left>
          <Body>
            <Title style={{ color : '#FFFFFF' }} > {name} </Title>
          </Body>
          <Right />
    </Header>

 export const getBackHeader   =  (navigation,name) => 
    <Header androidStatusBarColor="#008080" style={{ backgroundColor : '#008080',  borderBottomWidth : 0 }} >
          <Left>
          <Button transparent onPress={() => navigation.goBack()}>
            <Icon name="arrow-back" style={{ color : '#FFFFFF'}}/>
          </Button>
        </Left>
          <Body>
            <Title style={{ color : '#FFFFFF' }} > {name} </Title>
          </Body>
          <Right />
    </Header> 
