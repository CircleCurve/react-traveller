import React , { Component } from 'react' ; 
import  RoutePlan  from './RoutePlan'
import RouteDay from './RouteDay'
import { StackNavigator, TabNavigator } from 'react-navigation' ; 

export default ( Plan = StackNavigator ({
    Plan : { screen : RoutePlan }, 
    Day : { screen : RouteDay}
    //EditScreenTwo : { screen : EditScreenTwo} 
}))