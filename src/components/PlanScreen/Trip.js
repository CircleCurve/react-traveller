import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableHighlight,
    Animated,
    Easing,
    Image,
    Dimensions,
    Platform,
    ActivityIndicator, 
    ListView
} from 'react-native';
import SortableList from 'react-native-sortable-list';
import { Agenda } from 'react-native-calendars';
import ReservationsList from '../../../node_modules/react-native-calendars/src/agenda/reservation-list/index';

import Timeline from 'react-native-timeline-listview'

class CustomAgenda extends Agenda {

    renderReservations() {
        return (
            <CustomReservationsList
                rowHasChanged={this.props.rowHasChanged}
                renderItem={this.props.renderItem}
                renderDay={this.props.renderDay}
                renderEmptyDate={this.props.renderEmptyDate}
                reservations={this.props.items}
                selectedDay={this.state.selectedDay}
                topDay={this.state.topDay}
                onDayChange={this.onDayChange.bind(this)}
                onScroll={() => { }}
                ref={(c) => this.list = c}
                theme={this.props.theme}
            />
        );
    }
}


let data2 = [
      {time: '09:00', title: 'Event 1', description: 'Event 1 Description'},
      {time: '10:45', title: 'Event 2', description: 'Event 2 Description'},
      {time: '12:00', title: 'Event 3', description: 'Event 3 Description'},
      {time: '14:00', title: 'Event 4', description: 'Event 4 Description'},
      {time: '16:30', title: 'Event 5', description: 'Event 5 Description'}
    ]; 


class CustomReservationsList extends ReservationsList {
    render() {
        if (!this.props.reservations || !this.props.reservations[this.props.selectedDay.toString('yyyy-MM-dd')]) {
            return (<ActivityIndicator style={{ marginTop: 80 }} />);
        }
        return (
            /*
            <Timeline 
                data={data2} />
            */
            <CustomTimeLine
                data={data2}
                innerCircle={'icon'}
                
            />
            //<Basic style={this.props.style} />
            /*
            <FlatList
                ref={(c) => this.list = c}
                style={this.props.style}
                renderItem={this.renderRow.bind(this)}
                data={this.state.reservations}
                onScroll={this.onScroll.bind(this)}
                showsVerticalScrollIndicator={false}
                scrollEventThrottle={200}
                onMoveShouldSetResponderCapture={() => { this.onListTouch(); return false; }}
                keyExtractor={(item, index) => index}
            />*/
        );
    }
}

class CustomTimeLine extends Timeline {
    render() {
        
        return (
            <View style={[ {flex : 1 }, this.props.style]}>
            
                <SortableList
                    style= {{flex : 1 }} 
                    contentContainerStyle={styles1.contentContainer}
                    data={data1}
                    onChangeOrder= {(nextOrder) => { console.log("next order :" + nextOrder) } }
                    renderRow={ ({ data, active, index }) =>  this._renderRow (data, active, index)} 
                    //{...this.props.options}
                    /> 
                    { /*
                    <ListView
                        ref='listView'
                        style={[styles.listview]}
                        dataSource={this.state.dataSource}
                        renderRow={this._renderRow}
                        automaticallyAdjustContentInsets={false} 
                        {...this.props.options}
                    />*/ }
            </View>
                /*
            <View style={[styles.container, this.props.style]}>
                <ListView
                ref='listView'
                style={[styles.listview]}
                dataSource={this.state.dataSource}
                renderRow={this._renderRow}
                automaticallyAdjustContentInsets={false} 
                {...this.props.options}
                />
            </View>*/
        );
  }
}

/**
 * Sample React Native App
 * httpss://github.com/facebook/react-native
 * @flow
 */


const window = Dimensions.get('window');


const data1 = {
    /*
    0: {
        image: 'https://placekitten.com/200/240',
        text: 'Chloe',
    },
    1: {
        image: 'https://placekitten.com/200/201',
        text: 'Jasper',
    },
    2: {
        image: 'https://placekitten.com/200/202',
        text: 'Pepper',
    },
    3: {
        image: 'https://placekitten.com/200/203',
        text: 'Oscar',
    },
    4: {
        image: 'https://placekitten.com/200/204',
        text: 'Dusty',
    },
    5: {
        image: 'https://placekitten.com/200/205',
        text: 'Spooky',
    },
    6: {
        image: 'https://placekitten.com/200/210',
        text: 'Kiki',
    },
    7: {
        image: 'https://placekitten.com/200/215',
        text: 'Smokey',
    },
    8: {
        image: 'https://placekitten.com/200/220',
        text: 'Gizmo',
    },
    9: {
        image: 'https://placekitten.com/220/239',
        text: 'Kitty',
    },*/
                                   // <Icon active name={data.icon} style={{ color: "#777", fontSize: 26, width: 30 }} />

      0 : {time: '1', title: 'Archery Training', description: 'The Beginner Archery and Beginner Crossbow course does not require you to bring any equipment, since everything you need will be provided for the course. ',lineColor:'#009688', icon: require('../../img/archery.png')},
      1 : {time: '2', title: 'Play Badminton', description: 'Badminton is a racquet sport played using racquets to hit a shuttlecock across a net.', icon: require('../../img/badminton.png')},
      2 :{time: '3', title: 'Lunch', icon: require('../../img/lunch.png')},
      3 :{time: '4', title: 'Watch Soccer', description: 'Team sport played between two teams of eleven players with a spherical ball. ',lineColor:'#009688', icon: require('../../img/soccer.png')},
      4 : {time: '5', title: 'Go to Fitness center', description: 'Look out for the Best Gym & Fitness Centers around me :)', icon: require('../../img/dumbbell.png')}
   
};

class Basic extends Component {
    render() {
        return (

            <SortableList
                style={styles1.list}
                contentContainerStyle={styles1.contentContainer}
                data={data1}
                renderRow={this._renderRow}
                onChangeOrder= {(nextOrder) => { console.log(nextOrder)} } />
        );
    }

    _renderRow = ({ data, active }) => {
        return <Row data={data} active={active} />
    }
}

class Row extends Component {

    constructor(props) {
        super(props);

        this._active = new Animated.Value(0);

        this._style = {
            ...Platform.select({
                ios: {
                    transform: [{
                        scale: this._active.interpolate({
                            inputRange: [0, 1],
                            outputRange: [1, 1.1],
                        }),
                    }],
                    shadowRadius: this._active.interpolate({
                        inputRange: [0, 1],
                        outputRange: [2, 10],
                    }),
                },

                android: {
                    transform: [{
                        scale: this._active.interpolate({
                            inputRange: [0, 1],
                            outputRange: [1, 1.07],
                        }),
                    }],
                    elevation: this._active.interpolate({
                        inputRange: [0, 1],
                        outputRange: [2, 6],
                    }),
                },
            })
        };
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.active !== nextProps.active) {
            Animated.timing(this._active, {
                duration: 300,
                easing: Easing.bounce,
                toValue: Number(nextProps.active),
            }).start();
        }
    }

    render() {
        const { data, active } = this.props;

        return (
            <Animated.View style={[
                styles1.row,
                this._style,
            ]}>
                <Image source={{ uri: data.image }} style={styles1.image} />
                <Text style={styles1.text}>{data.text}</Text>
            </Animated.View>
        );
    }
}

const styles1 = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#eee',

        ...Platform.select({
            ios: {
                paddingTop: 20,
            },
        }),
    },

    title: {
        fontSize: 20,
        paddingVertical: 20,
        color: '#999999',
    },

    list: {
        flex: 1,
    },

    contentContainer: {
        width: window.width,

        ...Platform.select({
            ios: {
                paddingHorizontal: 30,
            },

            android: {
                paddingHorizontal: 0,
            }
        })
    },

    row: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#fff',
        padding: 16,
        height: 80,
        flex: 1,
        marginTop: 7,
        marginBottom: 12,
        borderRadius: 4,


        ...Platform.select({
            ios: {
                width: window.width - 30 * 2,
                shadowColor: 'rgba(0,0,0,0.2)',
                shadowOpacity: 1,
                shadowOffset: { height: 2, width: 2 },
                shadowRadius: 2,
            },

            android: {
                width: window.width - 30 * 2,
                elevation: 0,
                marginHorizontal: 30,
            },
        })
    },

    image: {
        width: 50,
        height: 50,
        marginRight: 30,
        borderRadius: 25,
    },

    text: {
        fontSize: 24,
        color: '#222222',
    },
});



export default class Trip extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: {}
        };
    }

    render() {
        return (
            <CustomAgenda
                // the list of items that have to be displayed in agenda. If you want to render item as empty date
                // the value of date key kas to be an empty array []. If there exists no value for date key it is
                // considered that the date in question is not yet loaded
                items={
                    this.state.items
                }
                // callback that gets called when items for a certain month should be loaded (month became visible)
                loadItemsForMonth={this.loadItems.bind(this)}                // callback that gets called on day press
                onDayPress={(day) => { console.log('day pressed'); }}
                // callback that gets called when day changes while scrolling agenda list
                onDayChange={(day) => { console.log('day changed') }}
                // initially selected day
                selected={'2012-05-16'}
                // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
                minDate={'2012-05-16'}
                // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
                maxDate={'2012-05-19'}
                // Max amount of months allowed to scroll to the past. Default = 50
                pastScrollRange={7}
                // Max amount of months allowed to scroll to the future. Default = 50
                futureScrollRange={0}
                // specify how each item should be rendered in agenda
                renderItem={this.renderItem.bind(this)}
                // specify how each date should be rendered. day can be undefined if the item is not first in that day.
                // specify how empty date content with no items should be rendered
                renderEmptyDate={this.renderEmptyDate.bind(this)}
                // specify how agenda knob should look like
                renderKnob={() => { return (<View />); }}
                // specify your item comparison function for increased performance
                rowHasChanged={this.rowHasChanged.bind(this)}
                // Hide knob button. Default = false
                hideKnob={true}
                firstDay={3}
                // By default, agenda dates are marked if they have at least one item, but you can override this if needed
                markedDates={{
                    '2012-05-16': { selected: true, marked: true },
                    '2012-05-17': { marked: true },
                    '2012-05-18': { disabled: true }
                }}
                // agenda theme
                theme={{
                    //...calendarTheme,
                    agendaDayTextColor: 'red',
                    agendaDayNumColor: 'green',
                    agendaTodayColor: 'red',
                    agendaKnobColor: 'blue'
                }}
            />
        )
    }

    loadItems(day) {
        console.log(this.state.items);
        setTimeout(() => {
            for (let i = 0; i < 7; i++) {
                const time = day.timestamp + i * 24 * 60 * 60 * 1000;
                const strTime = this.timeToString(time);
                if (!this.state.items[strTime]) {
                    this.state.items[strTime] = [];
                    const numItems = Math.floor(Math.random() * 5);
                    for (let j = 0; j < numItems; j++) {
                        this.state.items[strTime].push({
                            name: 'Item for ' + strTime,
                            height: Math.max(50, Math.floor(Math.random() * 150))
                        });
                    }
                }
            }
            //console.log(this.state.items);
            const newItems = {};
            Object.keys(this.state.items).forEach(key => { newItems[key] = this.state.items[key]; });
            this.setState({
                items: newItems
            });
        }, 1000);
        // console.log(`Load Items for ${day.year}-${day.month}`);
    }

    renderItem(item) {
        /*
        return (
            <View />
        )
        */

        return (

            <View style={[styles.item, { height: item.height }]}><Text>{item.name}</Text></View>

        );
    }

    renderEmptyDate() {

        return (
            <View style={styles.emptyDate}><Text>This is empty date!</Text></View>
        );
    }

    rowHasChanged(r1, r2) {
        return r1.name !== r2.name;
    }

    timeToString(time) {
        const date = new Date(time);
        return date.toISOString().split('T')[0];
    }
}

const styles = StyleSheet.create({
    item: {
        backgroundColor: 'white',
        flex: 1,
        borderRadius: 5,
        padding: 10,
        marginRight: 10,
        marginTop: 17
    },
    emptyDate: {
        height: 15,
        flex: 1,
        paddingTop: 30
    }
});