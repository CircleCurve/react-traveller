import React, { Component } from 'react';
import { Container, Header, Text, Tab, Tabs, ScrollableTab } from 'native-base';
//import ScrollableTabView, { ScrollableTabBar, DefaultTabBar} from 'react-native-scrollable-tab-view';

import { getHeader, getBackHeader } from '../HeaderScreen/Header';
import { onChangeDate, setPlanName } from '../../actions/routePlanAction';
import { connect } from 'react-redux';

import Trip from './Trip';
import Viewpoint from './Viewpoint';

class RouteDay extends Component {
    static navigationOptions = ({ navigation }) => ({
        header: (
            getBackHeader(navigation, navigation.state.params.planName)
        )
    });

    componentDidMount() {

    }


    constructor(props) {
        super(props);
        console.log(this.props.travelDay);
    }
    render() {
        return (
            <Container >
                <Tabs initialPage={0} tabBarBackgroundColor='#008080' tabBarUnderlineStyle={{ backgroundColor:'#fff'}}  renderTabBar={() => <ScrollableTab />}>
                    {
                        this.props.travelDay.map((day, index) => {
                            return <Tab  
                                    tabStyle={{backgroundColor: '#008080'}}
                                    textStyle={{color: '#fff'}}
                                    activeTabStyle={{backgroundColor: '#008080'}}
                                    activeTextStyle={{color: '#fff', fontWeight: 'bold'}}
                                    heading={"Week " + (index + 1)} 
                                    key={index}
                                    >
                                <Trip />
                            </Tab>
                        })
                    }
                </Tabs>
            </Container>
            /*
                   <Tab heading="Tab1">
                   </Tab>
                   <Tab heading="Tab2">
                   </Tab>
                   <Tab heading="Tab3">
                   </Tab>
               </Tabs>
           /*
           <ScrollableTabView
               initialPage={0}
               renderTabBar={() => <ScrollableTabBar />}
           >
               {
                   this.props.travelDay.map((day, index) => {
                       return <Trip tabLabel={"Week " + (index + 1)} key={index} />
                       //return <Text key={index} tabLabel={ "第" + (index + 1) + "日"}></Text>
                   })
               }
           </ScrollableTabView>*/
            /*
                <Tabs initialPage={0} >
                    {
                        this.props.travelDay.map( ( day , index) => {
                            return <Tab heading={"Week "+ (index + 1)  } key={index}>
                                        <Trip />
                                    </Tab>
                        })
                    }
                    { /*
                    <Tab heading="Tab1">
                    </Tab>
                    <Tab heading="Tab2">
                    </Tab>
                    <Tab heading="Tab3">
                    </Tab>
                </Tabs> */
            //<Viewpoint />
            //<Trip tabLabel={"Week 1"} /> */ }

        );
    }
}

const mapStateToProps = (state) => {
    return {
        sDate: state.routePlan.sDate,
        eDate: state.routePlan.eDate,
        planName: state.routePlan.planName,
        travelDay: state.routePlan.travelDay,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        onChangeDate: (type, sDate, eDate) => {
            dispatch(onChangeDate(type, sDate, eDate));
        },
        setPlanName: (name) => {
            dispatch(setPlanName(name));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RouteDay); 