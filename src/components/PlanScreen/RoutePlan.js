import React, { Component } from "react";
import { AppRegistry, Alert, View, Image } from "react-native";
import {
  Item,
  Input,
  Container,
  Card,
  CardItem,
  Body,
  Content,
  Header,
  Left,
  Right,
  Icon,
  Title,
  Button,
  Text
} from "native-base";
import { Col, Row, Grid } from 'react-native-easy-grid';
import DateTimePicker from 'react-native-modal-datetime-picker';
import { getHeader } from '../HeaderScreen/Header';

////=======Redux=======
import { onChangeDate, setPlanName, formatDate } from '../../actions/routePlanAction';
import { connect } from 'react-redux';

class RoutePlan extends Component {
  static navigationOptions = ({ navigation }) => ({
    header: (
      getHeader(navigation, "行程")
    )
  });
  constructor(props) {
    super(props);
    this.date = new Date();
    this.state = {
      isStartDateTimePickerVisible: false,
      isEndDateTimePickerVisible: false,

    };
  }

  _handleDatePicked = (sDate, eDate) => {
    console.log(sDate) ; 
    this.props.onChangeDate("start", sDate, eDate)
  };
  render() {
    return (
      <Container >
        <Content padder>
          <Card>
            <CardItem>
              <Item success>
                <Icon active name='home' />
                <Input placeholder='輸入行程標題' onChangeText={(text) => this.props.setPlanName(text)} />
                <Icon name='checkmark-circle' />
              </Item>
            </CardItem>
          </Card>
          <Card>
            <CardItem>
              <Grid>
                <Col size={1}>
                  <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                    <Text>由</Text>
                  </View>
                </Col>
                <Col size={9}>
                  <Button block light onPress={() => {
                    this.setState({
                      isStartDateTimePickerVisible: true
                    })
                  }} >
                    <Text>{ formatDate(this.props.sDate) }</Text>
                  </Button>
                </Col>
              </Grid>
              <DateTimePicker
                isVisible={this.state.isStartDateTimePickerVisible}
                onConfirm={ (date) => {
                  this._handleDatePicked(date, this.props.eDate) ; 
                  this.setState ({ isStartDateTimePickerVisible: false }) 
                }}
                onCancel={() => this.setState ({ isStartDateTimePickerVisible: false }) }
                cancelTextIOS={"取消"}
                confirmTextIOS={"確定"}
                minimumDate={this.date}
              />
            </CardItem>
            <CardItem>
              <Grid>
                <Col size={1}>
                  <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                    <Text>到</Text>
                  </View>
                </Col>
                <Col size={9}>
                  <Button block light onPress={() => {
                    this.setState({
                      isEndDateTimePickerVisible: true
                    })
                  }} >
                    <Text>{ formatDate(this.props.eDate) }</Text>
                  </Button>
                </Col>
              </Grid>
              <DateTimePicker
                isVisible={this.state.isEndDateTimePickerVisible}
                onConfirm={ (date) => {
                  this._handleDatePicked(this.props.sDate, date) 
                  this.setState ({ isEndDateTimePickerVisible: false }) 
                }}
                onCancel={() => this.setState ({ isEndDateTimePickerVisible: false }) }
                cancelTextIOS={"取消"}
                confirmTextIOS={"確定"}
                minimumDate={this.props.sDate}
              />
            </CardItem>
          </Card>
          <Button block success style={{ marginTop: 10 }} onPress={() => this.props.navigation.navigate("Day", { planName: this.props.planName })} >
            <Text>確定</Text>
          </Button>
        </Content>
      </Container >
    );
  }
}


const mapStateToProps = (state) => {
  return {
    sDate: state.routePlan.sDate,
    eDate: state.routePlan.eDate,
    planName: state.routePlan.planName
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    onChangeDate: (type, sDate, eDate) => {
      dispatch(onChangeDate(type, sDate, eDate));
    },
    setPlanName: (name) => {
      dispatch(setPlanName(name));
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RoutePlan);

