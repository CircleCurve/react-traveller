import {createStore, combineReducers, applyMiddleware} from 'redux'; 
import {createLogger} from 'redux-logger';
import thunk from 'redux-thunk' ; 
import {userReducer} from './reducers/userReducer' ; 
import {attractionReducer} from './reducers/attractionReducer' ; 
import {routePlanReducer} from './reducers/routePlanReducer' ; 


export default createStore(
    combineReducers({ 
        user : userReducer,
        attraction : attractionReducer,
        routePlan : routePlanReducer
    }),
    {},
    applyMiddleware(createLogger(), thunk)
);