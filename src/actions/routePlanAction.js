export const onChangeDate = (type, sDate, eDate) => {

    switch (type) {
      case "start" :
        if (sDate > eDate) {
          eDate = sDate ; 
        }
        break; 
      
      case "end" :
          

          break ; 
    }

    return { 
        type : "SET_PLAN_DATE", 
        payload : {
            sDate : sDate, 
            eDate : eDate, 
            travelDay : getAllDays (sDate, eDate)
        } 
    }
}

export const setPlanName = (name) => {
    return {
        type : "SET_PLAN_NAME",
        payload : {
            planName : name
        }
    }
}

export const formatDate = (dateTime) => {
    let date = new Date(dateTime) ; 

    let mm = date.getMonth() + 1; // getMonth() is zero-based
    let dd = date.getDate();

    return [date.getFullYear(),
            (mm>9 ? '' : '0') + mm,
            (dd>9 ? '' : '0') + dd
          ].join('-');
  }; 

function getAllDays(start ,end ) {
    var s = new Date(start );
    var e = new Date(end );
    var a = [];

    while(s < e) {
        a.push(s.toString());
        s = new Date(s.setDate(
            s.getDate() + 1
        ))
    }
    
    return a;
};
