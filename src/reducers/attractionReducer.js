import React from 'react' ; 
import {Image} from 'react-native' ; 
import { 
  Left, 
  Icon, 
  Right, 
  Button, 
  Body, 
  Content,
  Text, 
  Card, 
  CardItem, 
  Thumbnail } from "native-base";

export const attractionReducer = (state = {
    result : null
}, action) => {
    switch (action.type) {
        case "GET_ATTRACTION_API":
            locations = action.payload.XML_Head.Infos.Info ; 

            state = {
                ...state, 
                result : locations.filter( (location, index) => location.Picture1 !== "" )
                                  .filter ((location,index) => index < 10) 
                                  .map ( (location, index )=> {

                                    return (
                                        <Card key = {index} style={{ flex : 0 }}>
                                            <CardItem>
                                                <Left>
                                                <Thumbnail source={{uri: location.Picture1 }} />
                                                <Body>
                                                    <Text>{ location.Name }</Text>
                                                    <Text note>{ location.Add }</Text>
                                                </Body>
                                                </Left>
                                            </CardItem>
                                            {/*
                                                [location.Picture1, location.Picture2].map((picture,i) => (
                                                    <CardItem cardBody key={ picture }>
                                                        <Image  source={{uri: picture }} style={{height: 200, width: null, flex: 1}}/>
                                                    </CardItem>
                                                ) )
                                            */}
                                            
                                                <CardItem key={ location.Picture1 } cardBody >
                                                        <Image  source={{uri: location.Picture1 }} style={{height: 200, width : null, flex:1  }}/>
                                                </CardItem>
                                                
                                                <CardItem button>
                                                    <Body>
                                                        <Text>{ location.Description }</Text>
                                                    </Body>
                                                </CardItem>

                                            <CardItem>
                                                <Left>
                                                <Button transparent>
                                                    <Icon active name="thumbs-up" />
                                                    <Text>12 Likes</Text>
                                                </Button>
                                                </Left>
                                                <Body>
                                                <Button transparent>
                                                    <Icon active name="chatbubbles" />
                                                    <Text>4 Comments</Text>
                                                </Button>
                                                </Body>
                                                <Right>
                                                <Text>11h ago</Text>
                                                </Right>
                                            </CardItem>
                                        </Card> 
                                        ); 
                                    })  
            }
            break;
    
        default:
            break;
    }

    return state ; 
} 