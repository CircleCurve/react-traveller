import React, { Component } from 'react';
import HomeScreen from './src/components/HomeScreen/index';
//======Redux intergration================
import {Provider} from 'react-redux'; 
import store from './src/store'; 
//======Redux intergration================


export default class App extends Component {


  render () {

    return  (
      <Provider store={store}>
        <HomeScreen />    
      </Provider>
    )
  }
}